<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Faker\Generator;

/** @var \LaravelDoctrine\ORM\Testing\Factory $factory */
$factory->define(App\Entities\User::class, function (Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'rememberToken' => random_int(0, 1) ? str_random(10) : null,
    ];
});

$factory->define(\App\Entities\Product::class, function (Generator $generator) {
    return [
        'name' => $generator->words(random_int(1, 9), true),
        'price' => random_int(50, 20000),
        'unitsInStock' => random_int(0, 100),
    ];
});

$factory->define(\App\Entities\Shop\Category::class, function (Generator $faker) {
    return [
        'name' => $faker->words(random_int(2, 5), true),
    ];
});
