<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_categories', function (Blueprint $table) {
            $table->uuid('id');

            $table->integer('lft');
            $table->integer('rgt');
            $table->integer('lvl');
            $table->uuid('root')->nullable();
            $table->uuid('parent_id')->nullable();

            $table->string('name');

            $table->timestamps();

            $table->primary('id');

            $table->foreign('parent_id')
                ->references('id')
                ->on('shop_categories')
                ->onUpdate('cascade')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_categories');
    }
}
