<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        entity(\App\Entities\User::class, 5)->create();
        $products = entity(\App\Entities\Product::class, 20)->create();
        entity(\App\Entities\Shop\Category::class, 2)
            ->create()
            ->each(function (\App\Entities\Shop\Category $category) use ($products) {
                $children = entity(\App\Entities\Shop\Category::class, random_int(2, 4))->make();
                $children->each(function (\App\Entities\Shop\Category $child) use ($category, $products) {
                    $child->setParent($category);
                    $products->random(random_int(2, 10))->each(function (\App\Entities\Product $product) use ($child) {
                        $product->addCategory($child);
                    });
                    app('em')->persist($child);
                });
                app('em')->flush();
            });
    }
}
