<?php

namespace App\Entities\Shop;

use App\Entities\Product;
use App\Entities\Traits\UuidIdentifier;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Tree\Traits\NestedSetEntityUuid;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;

/**
 * Class Category
 * @ORM\Entity
 * @ORM\Table(name="shop_categories")
 * @Gedmo\Tree(type="nested")
 */
class Category
{
    use UuidIdentifier;
    use NestedSetEntityUuid;
    use Timestamps;

    /**
     * @var Category|null
     * @Gedmo\TreeRoot
     * @ORM\ManyToOne(targetEntity="App\Entities\Shop\Category")
     * @ORM\JoinColumn(name="root", referencedColumnName="id", nullable=true)
     */
    private $root;

    /**
     * @var Category|null $parent
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="App\Entities\Shop\Category", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="RESTRICT", nullable=true)
     */
    protected $parent;

    /**
     * @ORM\OneToMany(targetEntity="App\Entities\Shop\Category", mappedBy="parent")
     * @ORM\OrderBy({"left" = "ASC"})
     */
    protected $children;

    /**
     * @var string $name
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entities\Product", mappedBy="categories")
     */
    private $products;

    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getRoot()
    {
        return $this->root;
    }

    public function setParent(Category $parent = null)
    {
        $this->parent = $parent;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function addProduct(Product $product)
    {
        $this->products[] = $product;
    }
}
