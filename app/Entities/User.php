<?php

namespace App\Entities;

use Gedmo\Mapping\Annotation as Gedmo;
use App\Entities\Traits\UuidIdentifier;
use Doctrine\ORM\Mapping as ORM;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use LaravelDoctrine\ORM\Auth\Authenticatable as AuthTrait;
use LaravelDoctrine\Extensions\SoftDeletes\SoftDeletes;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;
use LaravelDoctrine\ORM\Notifications\Notifiable;


/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 **/
class User implements Authenticatable, CanResetPassword
{
    use AuthTrait;
    use Timestamps;
    use SoftDeletes;
    use \Illuminate\Auth\Passwords\CanResetPassword;
    use Notifiable;
    use UuidIdentifier;

    /**
     * @ORM\Column(type="string")
     * @var string $name
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     * @var string $email
     */
    protected $email;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }
}
