<?php

namespace App\Entities;
use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 */
class Notification extends \LaravelDoctrine\ORM\Notifications\Notification
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entities\User")
     */
    protected $user;
}
