<?php

namespace App\Entities;

use App\Entities\Shop\Category;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Entities\Traits\UuidIdentifier;
use Doctrine\ORM\Mapping as ORM;
use LaravelDoctrine\Extensions\SoftDeletes\SoftDeletes;
use LaravelDoctrine\Extensions\Timestamps\Timestamps;
use Money\Currency;
use Money\Money;

/**
 * Class Product
 *
 * @ORM\Entity
 * @ORM\Table(name="products")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class Product
{
    use UuidIdentifier;
    use Timestamps;
    use SoftDeletes;

    /**
     * @var string $name
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var int $price
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @var int $unitsInStock
     * @ORM\Column(name="units_in_stock", type="integer")
     */
    private $unitsInStock;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="App\Entities\Shop\Category", inversedBy="products")
     * @ORM\JoinTable(name="product_shop_categories")
     */
    private $categories;

    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param Money $price
     */
    public function setPrice(Money $price)
    {
        $this->price = $price->getAmount();
    }

    /**
     * @param int $unitsInStock
     */
    public function setUnitsInStock(int $unitsInStock)
    {
        $this->unitsInStock = $unitsInStock;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Money
     */
    public function getPrice(): Money
    {
        return new Money($this->price, new Currency('USD'));
    }

    /**
     * @return int
     */
    public function getUnitsInStock(): int
    {
        return $this->unitsInStock;
    }

    public function addCategory(Category $category)
    {
        $category->addProduct($this);
        $this->categories[] = $category;
    }
}
