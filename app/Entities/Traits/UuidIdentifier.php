<?php

namespace App\Entities\Traits;

use Doctrine\ORM\Mapping as ORM;

trait UuidIdentifier
{
    /**
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     * @var string $id
     **/
    protected $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}
